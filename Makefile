BUILD_DIR := ./build

FILES_IN := $(wildcard *.svg)

FILES_OUT_PNG_1X := $(addprefix $(BUILD_DIR)/, $(FILES_IN:.svg=.1x.png))
FILES_OUT_PNG_2X := $(addprefix $(BUILD_DIR)/, $(FILES_IN:.svg=.2x.png))
FILES_OUT_PNG_3X := $(addprefix $(BUILD_DIR)/, $(FILES_IN:.svg=.3x.png))
FILES_OUT_PNG := $(FILES_OUT_PNG_1X) $(FILES_OUT_PNG_2X) $(FILES_OUT_PNG_3X)

FILES_OUT_JPG_1X := $(addprefix $(BUILD_DIR)/, $(FILES_IN:.svg=.1x.jpg))
FILES_OUT_JPG_2X := $(addprefix $(BUILD_DIR)/, $(FILES_IN:.svg=.2x.jpg))
FILES_OUT_JPG_3X := $(addprefix $(BUILD_DIR)/, $(FILES_IN:.svg=.3x.jpg))
FILES_OUT_JPG := $(FILES_OUT_JPG_1X) $(FILES_OUT_JPG_2X) $(FILES_OUT_JPG_3X)

FILES_OUT := $(FILES_OUT_PNG) $(FILES_OUT_JPG)

.PHONY: all
all: $(FILES_OUT)

# PNG 1X
$(BUILD_DIR)/%.1x.png: %.svg | $(BUILD_DIR)
	@echo "Generating $@"
	@convert -background none -resize 64 $< $@

# PNG 2X
$(BUILD_DIR)/%.2x.png: %.svg | $(BUILD_DIR)
	@echo "Generating $@"
	@convert -background none -resize 256 $< $@

# PNG 3X
$(BUILD_DIR)/%.3x.png: %.svg | $(BUILD_DIR)
	@echo "Generating $@"
	@convert -background none -resize 1024 $< $@

# JPG 1X
$(BUILD_DIR)/%.1x.jpg: %.svg | $(BUILD_DIR)
	@echo "Generating $@"
	@convert -background white -resize 64 $< $@

# JPG 2X
$(BUILD_DIR)/%.2x.jpg: %.svg | $(BUILD_DIR)
	@echo "Generating $@"
	@convert -background white -resize 256 $< $@

# JPG 3X
$(BUILD_DIR)/%.3x.jpg: %.svg | $(BUILD_DIR)
	@echo "Generating $@"
	@convert -background white -resize 1024 $< $@

$(BUILD_DIR):
	@mkdir -p $@