# doip-artwork

Artwork and logos for the DOIP family of libraries

## Usage

Run `make` (or `make -B` on subsequent runs) to generate PNGs and JPGs from the SVG files.

## License

Images licensed under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).